import flask
from flask import Flask, render_template, request, url_for, Response,jsonify
import pytesseract
from PIL import Image
from flask_cors import CORS
import re
import random
import time 
from passporteye import read_mrz
import cv2
import numpy as np

app = flask.Flask(__name__)
CORS(app)
app.config["DEBUG"] = True
UPLOAD_FOLDER_OCR = 'ocr_images/'


@app.route('/ocr', methods=['POST'])
def home():
        imagefile = request.files.get('imagefile', '') 
        img = Image.open(imagefile)
        # pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'
        pytesseract.pytesseract.tesseract_cmd = r'/usr/bin/tesseract'
        time_now = int(time.time())
        random_digit  = random.randint(0,1000)
        file_name = UPLOAD_FOLDER_OCR + str(time_now) + '_' + str(random_digit) + '.jpeg'
        img.save(file_name,'PNG')
        img2 = cv2.imread(file_name,-1)

        dilated_img = cv2.dilate(img2, np.ones((7,7), np.uint8))
        bg_img = cv2.medianBlur(dilated_img, 21)
        diff_img = 255 - cv2.absdiff(img2, bg_img)
        norm_img = diff_img.copy()
        cv2.normalize(diff_img, norm_img, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8UC1)
        _, thr_img = cv2.threshold(norm_img, 230, 0, cv2.THRESH_TRUNC)
        cv2.normalize(thr_img, thr_img, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8UC1)


        # thr_img.save(file_name,'PNG')
        cv2.imwrite(file_name,thr_img)

        mrz = read_mrz(file_name)
        mrz_data = mrz.to_dict()
        name = re.split(r"\s+", mrz_data['surname'])
        data_set = {"name":name,"id_no":mrz_data['optional2'][0:8]}

        return data_set;
       
if __name__  == '__main__':
    app.run(host='127.0.0.1', port=8081, debug=True)

















